﻿namespace DrawSquare
{
    internal class Program
    {

        // Generates a square in a 2D array with a desired offset.
        public static char[,] GenerateSquare(char[,] square, int offset)
        {
            int rows = square.GetLength(1);
            int cols = square.GetLength(0);

            // Draw Top
            for (int x = offset; x < rows - offset; x++)
            {
                square[offset, x] = '*';
            }

            // Draw Bottom
            for (int x = offset; x < rows - offset; x++)
            {
                square[cols - 1 - offset, x] = '*';
            }

            // Draw left
            for (int y = offset; y < cols - offset; y++)
            {
                square[y, offset] = '*';
            }

            // Draw right
            for (int y = 0 + offset; y < cols - offset; y++)
            {
                square[y, rows - 1 - offset] = '*';
            }

            return square;
        }




        static void Main(string[] args)
        {

            // Square dimensions
            int dims = 0;

            bool inputInvalid = true;
            while (inputInvalid)
            {
                Console.WriteLine("Enter a number of rows and cols:");
                var input = Console.ReadLine();
                try
                {
                    dims = int.Parse(input!);
                    inputInvalid = false;
                }
                catch (Exception)
                {
                    Console.WriteLine("Invalid Input");
                }

            }

            // Todo: Implement rectangle, for now only a square is supported.
            // So both rows and cols have same dims.
            int rows = dims;
            int cols = dims;

            // Create empty 2D array for the square to generate
            char[,] Square = new char[rows, cols];

            Console.WriteLine($"Generating square of {dims}x{dims}");

            // First generate the outer square
            Square = GenerateSquare(Square, 0);
            // Next generate the inner square
            Square = GenerateSquare(Square, 2);

            // Draw the generated Square on console screen
            for (int x = 0; x < rows; x++)
            {
                for (int y = 0; y < cols; y++)
                {
                    if(Square[x, y] == 0)
                    {
                        Console.Write($"  ");
                    }
                    else
                    {
                        Console.Write($" {Square[x, y]}");
                    }
                }
                Console.Write($"\n");
            }
        }
    }
}